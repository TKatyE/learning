# Alphabetize sentence. 

def alphabetize(sentence)
  sent = sentence.split(" ")
  length = sent.length
  new_sent = []
  i = 0
  while i < length
    lc_word = sent[i].downcase
    new_sent.push(lc_word)
    i += 1
  end
  new_sent.sort!
  puts new_sent.join(" ")
end

sentence = ARGV.join(" ")
alphabetize(sentence)