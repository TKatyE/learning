#Load libraries
import matplotlib.pyplot as plt
from sklearn import datasets, svm, metrics

# Load the digits dataset.
digits = datasets.load_digits()

# Get the images - these are in digits.images and digits.targets as the labels. Each image is an 8x8 grid.
images_and_labels = list(zip(digits.images, digits.target))
# Iterate through the images and labels by getting each separately.
for index, (image, label) in enumerate(images_and_labels[10:14]):
    # Set up the plot for the training data.
    plt.subplot(2, 4, index + 1)
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Training: %i' % label)
    
# Flatten the image so the data is in an 8x8 matrix.
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))

# Create a classifer.
classifier = svm.SVC(gamma=0.001)

# Use the first half of the digits for learning.
classifier.fit(data[:n_samples /2], digits.target[:n_samples/2])

# Predict the value of a digit in the second half.
expected = digits.target[n_samples/2:]
predicted = classifier.predict(data[n_samples/2:])

print("Classification report for classifer %s:\n\%s\n" % (classifier, metrics.classification_report(expected, predicted)))
print("Confusion matris:\n%s" % metrics.confusion_matrix(expected, predicted))

images_and_predictions = list(zip(digits.images[n_samples/2:], predicted))
for index, (image, prediction) in enumerate(images_and_predictions[:4]):
    plt.subplot(2, 4, index + 5)
    plt.axis('off')
    plt.imshow(image, cmap=plt.cm.gray_r, interpolation='nearest')
    plt.title('Prediction: %i' % prediction)
    
plt.show()

