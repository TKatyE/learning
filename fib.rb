def fib(f1, f2)
  fn = f1 + f2
  return fn
end

def construct_fib(f0, f1, length)
  # Get first fn to start off the sequence.
  f2 = fib(f0, f1)
  fib_list = [f0, f1, f2]
  i = 3 # start the counter at 0.
  while i < length
    f_new = fib(f1, f2) # get the next number in the sequence
    f1 = f2 # set f1 as the previous f2
    f2 = f_new # set f2 as f_new
    fib_list.push(f_new) # add it to the existing list
    i += 1 # increase the counter
  end
  puts fib_list.join(" ")
    
end

f0 = ARGV[0].to_i
f1 = ARGV[1].to_i
length = ARGV[2].to_i

if f0.nil? || f1.nil? || length.nil?
  puts 'Please run as ruby fib.rb f0 f1 length'
else
  construct_fib(f0, f1, length)
end