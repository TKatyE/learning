#!/usr/local/bin/python

from distutils.core import setup

config = {
    'description': 'Letter Recognition',
    'author': 'Tara Sokolowski',
    'author_email': 'tara@sokolowski.com',
    'packages': ['letter']
}

setup(**config)
