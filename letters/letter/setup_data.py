#!/usr/local/bin/python

import sys, os, random

class SetupData(object):

#Set of functions to read in the data file and prepare it for processing.

    def __init__(self, datafile):
        # Return the datafile to be read.
        self.datafile = datafile

    def read_file(self):
        # Read in the datafile and store each line in an array.
        lines = [] # Empty array for lines
        # Open the file and read in each line
        fin = open(self.datafile, "r")
        for line in fin:
            line = line.strip("\n")
            lines.append(line)
        fin.close()
        # Return the array
        return lines
    
    def split_datasets(self, lines, train, test):
        # Randomly choose values to put in the train and test sets.
        train_sets = []
        # First get 16000 random indices to build the train list. As each is selected, remove it from the lines list.
        count = 0 # Set the initial train count at 0.
        # Shuffle the lines list around at random, remove the first elements and add them to train_sets.
        shuffled_lines = sorted(lines, key=lambda k: random.random())
        for x in shuffled_lines:
            if count < int(train):    
                x_index = lines.index(x)
                removed = lines[x_index]
                lines.pop(x_index)
                train_sets.append(removed)
                count += 1
            
        
        print len(train_sets) # Check length of training set matches train.
        print len(lines) # Check length of test set (in 'lines' list) matches test.
        print train_sets[1:10] # Check are lists.
        return (train_sets, lines) # Return the training set and test set as a tuple.
        
    def return_dict(self, sets):
        # Go through the training and testing dataset lists and convert to dictionaries with the expected result as the key and the integers as values.
        set_dict = {}
        for x in sets:
            expected = x[0]
            values = x[2:].split(',')
            set_dict[expected] = values
            
        print len(set_dict)
        return set_dict
        
    

path = "/Users/tara/Projects/learning/letters/letter/"
data_setup = SetupData(path + 'data/letter-recognition.data')
lines = data_setup.read_file()
sets = data_setup.split_datasets(lines, 16000, 4000)
train_sets = sets[0]
test_sets = sets[1]
        
    