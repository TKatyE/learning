# Read in a chunk of text and count the word frequency.

class WordFrequency

  def initialize(book, outfile, word)
    # Initialize book variable
    @book = book
    @outfile = outfile
    @word = word
  end
  
  def readText()
    count = 0
    File.open(@book, "r") do |f|
      f.each_line do |line|
        line_split = line.split()
        line_split.each do |word|
          if word.downcase.include? @word
            count += 1
          end
        end
      end
    end
    puts count
  end
  
  def prepare_word(word)
    # Put a word into lowercase and remove any extra non-characters.
    word = word.strip.downcase.gsub(/[^a-zA-Z]/, '')
    return word
  end

  def highestWordFrequency()
    word_freq = {} # Hash to store words with frequencies
    words = [] # Array for words already stored
    most_freq_word = ""
    count = 0
  
    File.open(@book, "r") do |f|
      f.each_line do |line|
        line_split = line.split()
        line_split.each do |word|
          prepare_word(word) # Put the word in lowercase and remove any non-letter characters.
          # Check if the word is already in the words array.
          if words.include? word
            current_freq = word_freq[word] # If it is, it is already in the word_freq hash. Get the value in the hash (the current frequency).
            new_freq = current_freq.to_i + 1 # Set the new frequency as the current frequency + 1.
            word_freq[word] = new_freq # Update the hash so the value for the word as the key is the new_freq.
            # If the new frequency is greater than the current highest frequency, replace the most_freq_word and count.
            if new_freq > count
              count = new_freq.to_i
              most_freq_word = word
            end
          else
            # If the word hasn't yet been seen, add it to the words list and to the hash with a freq (value) of 1.
            words.push(word)
            word_freq[word] = 1
          end
        end  
      end
    end
    
    puts "The most frequent word is " + most_freq_word + ", which appears " + count.to_s + " times."
      
  end
  
  def highestBigramFrequency()
    bi_freq = {} # Hash to store words with frequencies
    bis = [] # Array for words already stored
    most_freq_bi = ""
    count = 0
    prev_letter = ""
  
    File.open(@book, "r") do |f|
      f.each_line do |line|
        line.each do |letter|
          bi = prev_letter + letter
          
          # Check if the bi is already in the bis array.
          if bis.include? bi
            current_freq = bi_freq[bi] # If it is, it is already in the word_freq hash. Get the value in the hash (the current frequency).
            new_freq = current_freq.to_i + 1 # Set the new frequency as the current frequency + 1.
            bi_freq[bi] = new_freq # Update the hash so the value for the word as the key is the new_freq.
            
            # If the new frequency is greater than the current highest frequency, replace the most_freq_word and count.
            if new_freq > count
              count = new_freq.to_i
              most_freq_bi = bi
            end
          else
            # If the word hasn't yet been seen, add it to the words list and to the hash with a freq (value) of 1.
            bis.push(bi)
            bi_freq[bi] = 1
          end
          
          prev_letter = letter
        
        end  
      end
    end
    
    puts "The most frequent bigram is " + most_freq_bi + ", which appears " + count.to_s + " times."
      
  end
    
end


book_file = ARGV[0]
outfile = ARGV[1]
path = ARGV[2]
word = ARGV[3]

b = WordFrequency(path + book_file, path + outfile, word)

#b.readText()
b.highestWordFrequency()
b.highestBigramFrequency()
