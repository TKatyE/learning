# Reverse words in a string.

def reverse_words(sentence)
  # Get an empty list for the new, reversed sentence.
  rev = []
  # Turn the string into an array.
  sent = sentence.split(" ")
  # Get the length of the sentence.
  length = sent.length
  # Take the last element of the array and add it to the new array.
  i = 0
  while i < length
    last_word = sent.pop
    rev.push(last_word)
    i += 1
  end
  puts rev.join(" ")
end

sentence = ARGV.join(" ")
if sentence.nil?
  puts "Please provide a string to reverse."
else
  reverse_words(sentence)
end